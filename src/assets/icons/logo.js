// Example of SVG converted to js array, so it can be used with CIcon.
// the first argument is two last values of svg viewBox, 
// the second argument is the SVG content stripped of SVG tags
export const logo = ['556 134',`
  <g>
    <g style="fill:#1bbd93;"></g>
    <g style="fill:currentColor;">
      <g></g>
      <g>
      </g>
    </g>
  </g>
`]
